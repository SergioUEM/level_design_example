﻿using System.Text;
using UnityEngine;

public class GameLoopsTest : MonoBehaviour {

    StringBuilder sb = new StringBuilder();
	
	private void Update () {
        sb.Remove(0, sb.Length);
        sb.Append("Update Delta Time: ");
        sb.Append(Time.deltaTime);
        print(sb.ToString());
	}


    private void LateUpdate() {
        sb.Remove(0, sb.Length);
        sb.Append("Late Update Delta Time: ");
        sb.Append(Time.deltaTime);
        print(sb.ToString());
    }


    private void FixedUpdate() {
        sb.Remove(0, sb.Length);
        sb.Append("Fixed Update Delta Time: ");
        sb.Append(Time.fixedDeltaTime);
        print(sb.ToString());
    }
}
